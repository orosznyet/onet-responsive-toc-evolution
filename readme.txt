=== ONet Responsive TOC Evolution ===
Contributors: József Koller
Donate link: http://onetdev.com/repo/onet-responsive-toc-evo
Tags: responsive, toc, content, overlay, navigation, auto, anchor, modern
Requires at least: 3.5
Tested up to: 3.6.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Get a dynamicly generated table of contents (navigation) overlay for each of your post and pages instatntly.

== Description ==

Are you one of those people who likes to publish long articles? Assuming your answer is yes I highly recommend you to keep reading this otherwise you may leave.

In nutshell this plugin automatically generates "table of contents overlay" (TOC) for posts and pages using their headlines. It does not matter if your post is 7 years old or didn't published yet because you don't need to "scan" your existing contets to get the TOC overlay work. It will do the job when a user request a post which has the "display TOC overlay" option on. Yes, you can turn on/off this feature for each posts and pages. (Check out the overlay demos in screenshots section.)

If it was not enough already you can download additional themes from the author.

Main features:

*   Generating table of contents for each posts/pages.
*   Does not consume additional database storage.
*   Theme support.
*   Works with custom post types.
*   Overlay can be turned of globally or individually for each posts and pages.
*	Touch screen support.
*   Keyboar navigation support.
*   Multilanguage support.
*   Advanced settings panel.

Feature list for developers:

*   Ability to modify TOC overlay instances.
*   Ability to init own overlays (detailed configurations)

== Installation ==

1. Install ONet Responsive TOC Evo either via the WordPress.org plugin directory, or by uploading the files to your server
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Chose a method listed in description.

== Frequently Asked Questions ==

= Where I can find the options in post editor? =

Click on "Screen Options" in post editor > check the "Table of Content settings"

== Screenshots ==

1. /assets/screenshot-1.png
2. /assets/screenshot-2.png
3. /assets/screenshot-3.png

== Changelog ==

= 1.0 =
* Initial release

== Upgrade Notice ==

= 1.0 =
Initial release