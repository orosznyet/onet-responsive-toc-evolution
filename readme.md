# ONet Responsive TOC Evolution

This repo contains all the files for "ONet Responsive TOC Evolution" which is a Wordpress plugin released under [GPLv2](http://www.gnu.org/licenses/gpl-2.0.html) licence.

Are you one of those people who likes to publish long articles? Assuming your answer is yes I highly recommend you to keep reading this otherwise you may leave.

In nutshell this plugin automatically generates "table of contents overlay" (TOC) for posts and pages using their headlines. It does not matter if your post is 7 years old or didn't published yet because you don't need to "scan" your existing contets to get the TOC overlay work. It will do the job when a user request a post which has the "display TOC overlay" option on. Yes, you can turn on/off this feature for each posts and pages. (Check out the overlay demos in screenshots section.)

If it was not enough already you can download additional themes from the author.

**Main features**:

*   Generating table of contents for each posts/pages.
*   Does not consume additional database storage.
*   Theme support.
*   Works with custom post types.
*   Overlay can be turned of globally or individually for each posts and pages.
*	Touch screen support.
*   Keyboar navigation support.
*   Multilanguage support.
*   Advanced settings panel.

Feature list for developers:

*   Ability to modify TOC overlay instances.
*   Ability to init own overlays (detailed configurations)

## Credits

[József Koller](http://profiles.wordpress.org/orosznyet/) - Main codebase

**[Donate link](http://onetdev.com/repo/onet-responsive-toc-evo)**

## Download

You can download the Wordpress plugin repo just click on the download link. **[Download here](http://wordpress.org/plugins/onet-responsive-toc-evo/)**

## Installation

1. Install ONet Responsive TOC Evo either via the WordPress.org plugin directory, or by uploading the files to your server (download link above).
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Chose a method listed in description.

## Changelog

### 1.0
* Initial release