<?php  
/* 
Plugin Name: ONet Responsive TOC Evolution
Plugin URI: http://www.orosznyet.com/community/#oRTE
Description: Get a dynamicly generated table of contents (navigation) overlay for each of your post and pages instatntly.
Author: József Koller
Version: 1.1
Author URI: http://www.orosznyet.com/
Text Domain: orte
Domain Path: /lang
*/ 

/**
* Installation function, basicly it creates 
* @since 0.1
**/
register_activation_hook( __FILE__, "onet_RTE_install" );
function onet_RTE_install () {
global $wp,$wpdb,$ONetRespTOCEvo_inst;
	// Load settings if already installed early
	$opts = get_option( "onet_rtoce_settings", array() );
	if (!is_array($opts)) $opts = array();
	update_option( "onet_rtoce_settings", array_merge($opts,array("custom_id" => rand(100,999)*rand(200,500))) );
}


/**
* Register init function
* @since 0.1
**/
add_action( 'init', 'ONetRespTOCEvo_wrap' );
function ONetRespTOCEvo_wrap() {
global $ONetRespTOCEvo_inst;
	require_once(dirname(__FILE__)."/exception.php"); // import exception class extension
	$ONetRespTOCEvo_inst = new ONetRespTOCEvo();
	$ONetRespTOCEvo_inst->init();
}


/**
* The main code stuff. Hell yeah.
* @since 0.1
**/
class ONetRespTOCEvo {
	private $opts;
	private $cap;
	private $admHook;
	private $display_pointer;
	private $prefix;
	
	/**
	* Construct this instance.
	* This function can be called anywhere in WP.
	* @since 0.1
	**/
	public function init () {
	global $wpdb,$wp;
		// Basic variables and contants
		define("ORTE_OPT_PREFIX", "orte_");                                    // option prefix, widget cache prefix
		define("ORTE_VER",1.1);                                                // Version number
		define("ORTE_PLUGIN_NAME", basename(__DIR__) );                        // Plugin name (based on folder name)
		define("ORTE_THEME_DIR", dirname(__FILE__)."/themes");
		define("ORTE_THEME_URL", site_url("wp-content/plugins/".ORTE_PLUGIN_NAME."/themes"));
		define("ORTE_URL", site_url("wp-content/plugins/".ORTE_PLUGIN_NAME));
		define("ORTE_PATH", ABSPATH."wp-content/plugins/".ORTE_PLUGIN_NAME);
		$plugin = plugin_basename( __FILE__ );
		$this->prefix = ORTE_OPT_PREFIX;
		$this->get_opts();

		// Load language
		load_plugin_textdomain('orte', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

		// Register actions
		add_action('admin_menu', array($this,'reg_admin_menu') );                          // Admin: Register settings page
		add_action('admin_enqueue_scripts', array($this,'admin_enqueue_scripts') );        // Admin: Enqueue scripts
		add_action('widgets_init', create_function('', 'return register_widget("oRTE_Widget");') ); // Admin&Client: Create instance for widget
		add_action('add_meta_boxes', array($this,'prepare_coredit_meta') );                // Admin: Add metabox 
		add_action('save_post', array($this,'save_coreditor_meta') );                      // Admin: Save metabox settings
		add_action('wp_enqueue_scripts', array($this,'post_script_register') );            // Preregister scripts for client side
		add_action('get_footer', array($this,'post_process'));                             // Client: Adds the post category detector also this runs the overlay call

		// Register filters
		add_filter('plugin_action_links_'.$plugin,array($this, 'plugin_page_link'));       // Admin: settings link for plugins page
		add_filter('contextual_help', array($this,'admin_help_content'), 10, 3);           // Admin: Help tabs for main admin UI
		add_filter('post_class', array($this,"post_class") );                              // Client: post class filter for JS

		// Capabilities
		$this->cap = apply_filters( 'onet_tocevo', 'manage_options' );

	}


	/*********************************
	* Load and save options
	*********************************/


	/**
	* Default settings
	* @since 0.1
	* @param void
	* @return (array) default variables
	**/
	function get_opts_default() {
		return array(
			"custom_id" => "pointer", // This value will be overwritten on install with a random number.
			"global" => 1,
			"displayfor" => array("post","page"),
			"title" => __("Bookmark","orte"),
			"theme" => "default",
			"autoopen" => 0,
			"swipeopen" => 1,
			"swipeorigin" => "right",
			"hidebutton" => 0,
			"hotkeys" => 1,
			"manual" => 0
		);
	}

	/**
	* Load settings from database (use this instead of direct access for WP function)
	* If chache is available ($this->opts) then returns with that if force update is not true
	* @since 0.1
	* @param (boolean) foorce fetch from database
	* @return (array) stored settings
	**/
	function get_opts($force_update=false) {
	global $wp;
		if  (is_array($this->opts) && !$force_update) {
			return $this->opts;
		} else {
			$opts = get_option( "onet_rtoce_settings", null ); // get stored value from WP
			$return = array();
			if ( !is_array($opts) ) $return = $this->get_opts_default();
			else $return = array_merge($this->get_opts_default(),$opts);
			$this->opts = $return;
			return $this->opts;
		}
	}

	/**
	* Put new setting(s) into WP
	* @since 0.1
	* @param (array) new settings values, [(boolean) if previous arg is complementary or overwrite update]
	* @return (array) stored settings
	**/
	function set_opts ($opts,$overwrite=false) {
		if (!is_array($opts)) throw new ONet_ResTOCEvo_Exception("Given first parameter is not array.");
		$new_opts = array();

		// Update priority: default < stored settings < new option(s)
		if ($overwrite == false) $new_opts = array_merge($this->get_opts_default(), $this->get_opts(), $opts);
		// Update priprity: default < new options
		else $new_opts = array_merge($this->get_opts_default(), $opts);

		// Put new settings into WP
		update_option( "onet_rtoce_settings", $new_opts );
		$this->opts = $new_opts;
		return true;
	}


	/*********************************
	* Administration interface
	*********************************/


	/**
	* Enqueue admin scripts and styles (including pointer when it is needed.)
	* This function called by WP.
	* @since 0.1
	**/
	function admin_enqueue_scripts () {
	global $current_user, $wp_version;

		// Check if pointer should be displayed
		$dismissed = explode( ',', (string) get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );
		if (version_compare($wp_version, '3.5', '>=') && !in_array('orte_point'.$this->opts['custom_id'], $dismissed) && current_user_can($this->cap)) {
			$this->display_pointer = true;
		} else {
			$this->display_pointer = false;
		}

		// Register scripts
		wp_register_script('onet-rte-adminmain', plugins_url( 'assets/js/admin.main.js', __FILE__ ), array("jquery"),"1.0",true);
		wp_register_script('onet-rte-pointer', plugins_url( 'assets/js/admin.pointer.js', __FILE__ ), array("jquery"),"1.0",true);
		wp_register_script('jquery-ui-toggleswitch', plugins_url( 'assets/js/jquery-ui.toggleSwitch.min.js', __FILE__ ), array("jquery"),"1.0",false);
		wp_register_script('onet-modernizr', plugins_url( 'assets/js/modernizr.js', __FILE__ ), array("jquery"),"1.0",false);
		wp_register_style ('onet-main', plugins_url( 'assets/css/main.css', __FILE__ ));

		// Localisation
		$lang = array(
			"text" => '<h3>'.__("Responsive TOC Evolution is now installed and ready to use!", "orte").'</h3>' .
				'<p>'.__("There are some settings you should check first.", "orte").'</p>' .
				'<p>'.__("You can also check pregenerated bookmark in post editor (edit the post which you want to check).", "orte").'</p>',
			"customid" => $this->opts['custom_id'],
			"menuname" => basename(__FILE__) );
		wp_localize_script('onet-rte-pointer', 'orte_pointer_loc', $lang );

		$lang = array(
			"lang" => array(
				"on" => __("on","orte"),
				"off" => __("off","orte"),
				"yes" => __("yes","orte"),
				"no" => __("no","orte")
				),
			"prefix" => ORTE_OPT_PREFIX
			);
		wp_localize_script('onet-rte-adminmain', 'orte_admin_loc', $lang );

		// Enqueue scripts
		if ($this->display_pointer == true) {
			wp_enqueue_script('wp-pointer');
			wp_enqueue_script('onet-rte-pointer');
			wp_enqueue_style ('wp-pointer');
		}
		wp_enqueue_script('jquery-ui-toggleswitch');
		wp_enqueue_script('onet-rte-adminmain');
		wp_enqueue_script('onet-modernizr');
		wp_enqueue_style('onet-main');
	}

	/**
	* Prepare variables for admin ui then display it
	* Called by WP
	* @since 0.1
	* @param void
	* @return void
	**/
	function prepare_admin_ui () {
	global $wpdb,$wp_version;
		// Base variables
		$prefix = ORTE_OPT_PREFIX;
		$updates = array();
		$postTypes = $this->get_post_types();
		
		## Saving datas
		if (!empty($_POST) && isset($_POST['orte_nonce_validator']) && wp_verify_nonce( $_POST['orte_nonce_validator'], 'orte_nonce'.$this->opts['custom_id'] ) ) {
			$new_opts = array();
			
			// Checkbox check. Lol, this one sounds silly.
			$chk = explode(";","global;autoopen;swipeopen;hidebutton;hotkeys;manual");
			foreach ($chk AS $val) {
				if (isset($_POST[$prefix.$val])) $new_opts[$val] = 1;
				else $new_opts[$val] = 0;
			}
			// Textbox check.
			$txt = explode(";","title;swipeorigin;theme");
			foreach ($txt AS $val) {
				$new_opts[$val] = isset($_POST[$prefix.$val]) ? $_POST[$prefix.$val] : "";
			}

			// Post type settings
			$postTypes = $this->get_post_types();
			$new_opts['displayfor'] = array();
			foreach ($postTypes as $type => $meta) {
				if (isset($_POST[$prefix."displayfor"]) && isset($_POST[$prefix."displayfor"][$type])) $new_opts['displayfor'][] = $type;
			}

			// Save theme settings
			$themes = $this->list_valid_themes();
			foreach ($themes AS $theme => $settings) {
				if (isset($_POST[$prefix.'theme_'.$theme]) && is_array($_POST[$prefix.'theme_'.$theme]) && is_array($settings['opts'])) {
					$new_opts['theme_'.$theme] = array();
					$_POST_theme;
					foreach ($settings['opts'] AS $tset => $tval) {
						if ($tval['type'] == "checkbox") $new_opts['theme_'.$theme][$tset] = isset($_POST_theme[$tset]);
						else if ($tval['type'] == "select")
							$new_opts['theme_'.$theme][$tset] = (isset($_POST_theme[$tset]) && isset($tval['options'][$_POST_theme[$tset]])) ? : $tval['default'];
						else $new_opts['theme_'.$theme][$tset] = isset($_POST_theme[$tset]) ? $_POST_theme[$tset] : null;
					}
				} else {
					$new_opts['theme_'.$theme] = array();
				}
			}

			// Update settings
			$this->set_opts($new_opts);
			$updates[] = __("Settings updated.","orte");
		}

		// Load themes and related settings.
		$themes = $this->list_valid_themes();
		$theme_settings = array();
		foreach ($themes as $theme => $meta) $theme_settings[$theme] = $this->generate_theme_settings($theme,$meta['opts']);

		// Execute the UI output
		$pass = $this;
		require_once(dirname(__FILE__)."/view/admin.php");
	}

	/**
	* Draws a custom meta
	* @since 0.1
	* @param (string) element ID
	* @param (string) box title
	* @param (string) box content
	**/
	function admin_custometa ($id, $title, $content) {
		require(dirname(__FILE__)."/view/admin.custometa.php");
	}

	/**
	* Generates box and echo support in admin
	* @since 0.1
	* @param void
	* @return void
	**/
	function admin_custometa_support () {
		$mail = "orosznyet@gmail.com".
			"?subject=".urlencode(__("[SUPPORT] Responsive TOC Evolution","orte")).
			"&amp;body=".urlencode(sprintf(__("Describe the issue: \n\nThe following informations are really important\nScript version: %s\nWebsite: %s\nPHP Version: %s\ncURL support: %s\n","orte"),ORTE_VER,get_site_url(),phpversion(),function_exists("curl_init") ? __("supported","orte") : __("not supported","orte")));

		$this->admin_custometa(
			"orte-troubleshoot",
			__("Contact and Support", "orte"),
			__('Thank you for your purchase, we hope you like Responsive TOC Evolution for Wordpress. If you have any question, problem or suggestion feel free to contact me. You can access quick help for almost each options if you move your mouse over the "?" icons. Also you can find "Read me" if you click on "Help" button above this box.', "orte")."<br/><br/><a href='mailto:".$mail."' target='_blank'>orosznyet@gmail.com</a>"
			);
	}

	/**
	* Generates box and echo premium support
	* @since 0.1
	* @param void
	* @return void
	**/
	function admin_custometa_premiumsupport () {
		$mail = "orosznyet@gmail.com".
			"?subject=".urlencode( __("[PREMIUM SUPPORT] Responsive TOC Evolution","orte") );

		$this->admin_custometa(
			"orte-premiumsupport",
			__("Premium Support", "orte"),
			sprintf(
				__('The plugin developer offers premium support for low hourly rate. If you have special queries feel free to contact the author via %s.','orte'),
				'<a href="mailto:'.$mail.'" target="_blank">orosznyet@gmail.com</a>')
			);
	}

	/**
	* Init the metabox
	* @since 0.1
	* @param (obj) post data
	* @return (boolean) always true
	**/
	function prepare_coredit_meta() {
	global $post;
		if ($this->opts['global'] != 1) return;

		$post_types = get_post_types( '', 'names' );
		if (!empty($post) && in_array($post->post_type,$post_types) ) {

			foreach ($post_types AS $screen) :
				if (in_array($screen, $this->opts['displayfor'])) :
					add_meta_box(
						'orte_coreditor_meta_dom',
						__( 'Table of Content settings', 'orte'),
						array($this,"coreditor_meta_ui"),
						$screen,
						'side',
						'core'
					);
				endif;
			endforeach;
		}
		return true;
	}

	/**
	* The content of metabox
	* @since 0.1
	* @param (obj) post data
	* @return void
	**/
	function coreditor_meta_ui ($post) {
	global $wpdb, $post;
		wp_nonce_field(plugin_basename( __FILE__ ), 'orte_coreditor_nonce' );

		$opts = get_post_meta($post->ID, ORTE_OPT_PREFIX."settings", true );
		if (!is_array($opts) || empty($opts)) $opts = array();
		$opts = array_merge( array("overlay"=>2,"manual"=>0), $opts);
		require_once(dirname(__FILE__)."/view/coreditor.meta.php");
	}

	/**
	* Saving the metabox content
	* @since 0.1
	* @param (int) post ID
	* @return (boolean) if update was success
	**/
	function save_coreditor_meta ($post_id) {
	global $post;
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) { return; }
		if (!wp_verify_nonce( $_POST['orte_coreditor_nonce'], plugin_basename(__FILE__))) { return; }
		if (!current_user_can( 'edit_post', $post_id ) || !current_user_can( $this->cap, $post_id ) ) { return; }
		if ($this->opts['global'] != 1) return;

		$upd = array();
		if (isset($_POST[ORTE_OPT_PREFIX."single_overlay"])) {
			$upd['overlay'] = (int)$_POST[ORTE_OPT_PREFIX."single_overlay"];
			if ($upd['overlay'] < 0 || $upd['overlay'] > 2) $upd['overlay'] = 2;
		}

		if (isset($_POST[ORTE_OPT_PREFIX."manual_init"])) {
			$upd['manual'] = (int)$_POST[ORTE_OPT_PREFIX."manual_init"];
			if ($upd['manual'] < 0 || $upd['manual'] > 1) $upd['manual'] = 0;
		}

		// Update settings array
		update_post_meta($post->ID,ORTE_OPT_PREFIX."settings", $upd);
		return true;
	}

	/**
	* Append custom settings link to plugin admin
	* @since 0.1
	* @param (array) links
	* @return (array) update links
	**/
	function plugin_page_link( $links ) {
		if (current_user_can( $this->cap)) array_push( $links, '<a href="'.$this->admin_uri().'">'.__("Settings","orte").'</a>' );
		return $links;
	}

	/**
	* Registering administration interface link
	* @since 0.1
	* @param void
	* @return (obj) administration hook
	**/
	function reg_admin_menu() {
		$this->admHook = add_options_page(__('ONet Responsive TOC Evolution','orte'),__('Resp. TOC Evolution','orte'),$this->cap,ORTE_PLUGIN_NAME,array($this,"prepare_admin_ui"));
		return $this->admHook;
	}

	/**
	* Adds readme andwelsome message to plugin admin page
	* Called directly from WP, do not call manually
	* @since 0.1
	**/
	function admin_help_content ($contextual_help, $screen_id, $screen) {
		if ($screen_id == $this->admHook) {
			$screen->add_help_tab( array(
				'id'      => 'orte-welcome',
				'title'   => __('Welcome', "orte"),
				'content' => __('<h2>Dear user</h2>Thank you for your using this plugin, we hope you like Responsive TOC Evolution for Wordpress. If you are looking for troubleshoot, feature list, known issues, please click on "Read me" from the left menu. (Readme and support available in english only.)', "orte")
				)
			);
			// Display readme from file (added line breaks)
			$screen->add_help_tab( array(
				'id'      => 'orte-readme',
				'title'   => __('Readme', "orte"),
				'content' => nl2br(file_get_contents( dirname(__FILE__) . '/readme.txt' ))
				)
			);
		}
		return '';
	}


	/*********************************
	* Clientside stuff
	*********************************/


	/**
	* Adds the class to the post we need. Called directly by wp
	* @since 0.1
	* @param (array) class set
	* @return (array) updated class set
	**/
	function post_class($classes) {
		$classes[] = $this->prefix.'post';
		return $classes;
	}

	/**
	* Register cliend side scripts
	* @since 0.1
	* @param void
	* @return void
	**/
	function post_script_register () {
		wp_register_script('onet-rte-tocjs', plugins_url( 'assets/js/jquery.onet.tableofcontents.js', __FILE__ ), array("jquery"),"1.0",true);
		wp_register_script('onet-rte-client', plugins_url( 'assets/js/client.js', __FILE__ ), array("jquery","onet-rte-tocjs"),"1.0",true);

		// Adds important variables to client
		$lang = array(
			"lang" => array(),
			"theme_name" => $this->opts['theme'],
			"theme_settings" => array_merge($this->get_theme_defaults(), is_array($this->opts["theme_".$this->opts['theme']]) ? $this->opts["theme_".$this->opts['theme']] : array()),
			"header_text" => $this->opts['title'],
			"prefix" => $this->prefix,
			"pluginbase" => ORTE_URL
			);
		wp_localize_script('onet-rte-tocjs', 'orte_client_loc', $lang );
	}

	/**
	*
	**/
	function post_process () {
	global $wpdb, $post;

		if (!is_single()) return;                                      // RETURN! First of all, check if the page is single or not.
		$opts = $this->get_opts(true);                                 // Get the latest settings
		$postID = get_the_ID();                                        // Try to get post id
		if ($opts['global'] != 1) return;                              // RETURN! If global settings is off
		if ((int)$postID < 1) return;                                  // RETURN! In case of missing post ID the script should not proceed
		$postType = get_post_type($postID);                            // Get the post type (I know, i could use $post->post_type).
		$postOpts = get_post_meta($postID, 'orte_settings');           // Fetch post settings
		if (!is_array($postOpts)) $postOpts = array();                 // Fix missing/invalid settings meta
		$postOpts = array_merge(array("overlay"=>0,"manual"=>0));      // Complete meta
		if ($postOpts['overlay'] == 2) return;                         // RETURN! if overlay blocked by the post individually
		
		// Okey, if the overlay is not forced to display continue the process with the basic stuff
		if ($postOpts['overlay'] != 1) {
			if (!in_array($postType, $opts['displayfor'])) return;     // RETURN! When posttype is forbidden
		}

		// Enqueue main javascript ;)
		wp_enqueue_script('onet-rte-tocjs');

		// Check for manual init
		if ($postOpts['manual'] == 2) return;
		if ($postOpts['manual'] != 1 && $opts['manual'] == 1) return;
		wp_enqueue_script('onet-rte-client');                           // Execute the script
	}


	/*********************************
	* Utility methods
	*********************************/


	/**
	* Returns with a direct link to admin
	* @since 0.1
	* @param [(assoc array) additional link params]
	* @param [(boolean) use &amp; or &]
	**/
	public function admin_uri ($params=array(),$amp=true) {
		$link = admin_url( "tools.php?page=".ORTE_PLUGIN_NAME );
		if (count($params)) {
			$link .= $amp ? "&amp;" : "&";
			$new_params = array();
			foreach ($params AS $key => $val) $new_params[] = $key."=".(string)$val;
			$link .= implode($amp ? "&amp;" : "&",$new_params);
		}
		return $link;
	}

	/**
	* Read a directory
	* @since 0.1
	* @param (string) folder path
	* @param (boolean|int) recursive depth (false if only one level)
	* @return (array) match set
	**/
	function readDirs ($folder,$deep=false) {
		if (!preg_match("/(\/|\\\\)$/i", $folder)) $folder = $folder."/";
		$deep = intval($deep) > 0 ? intval($deep) : $deep = false;
		$set = array( "dir"=>array(), "file"=>array() );

		if ($handle = opendir($folder)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					if (is_dir($folder.$entry)) {
						if ($deep == false) $set['dir'][] = $entry;
						else $set['dir'][$entry] = $this->readDirs($folder.$entry,$deep-1);
					}
					else if (is_file($folder.$entry)) $set['file'][] = $entry;
				}
			}
			closedir($handle);
		}
		return $set;
	}

	/**
	* Search valid themes
	* @since 0.1
	* @param (boolean) check if listing can be cached or not
	* @return (array) list of valid themes
	**/
	function list_valid_themes ($forced=false) {
		if (is_array($_SERVER['orte_GLOBAL']) && $forced != false) return $_SERVER['orte_GLOBAL'];

		$elems = $this->readDirs(ORTE_THEME_DIR,1);
		$_SERVER['orte_GLOBAL'] = array();

		if (count($elems['dir']) > 0) {
			foreach ($elems['dir'] AS $dir => $cont) {
				if (count($cont['file']) > 0) {
					// If all required file is exsists
					$files = array_flip($cont['file']);
					if (isset($files['styles.css']) && isset($files['methods.js']) && isset($files['meta.php']) && isset($files['screen.jpg'])) {
						require ORTE_THEME_DIR."/".$dir."/meta.php";
						$_SERVER['orte_GLOBAL'][$dir] = $settings;
					}
					unset($files);
				}
			}
		}

		return $_SERVER['orte_GLOBAL'];
	}

	/**
	* Generate theme settings HTML output
	* @since 0.1
	* @param (string) theme nme
	* @param (array) options array
	**/
	function generate_theme_settings ($theme,$opts) {
		if (!is_array($opts)) return "";

		// Basic variables
		$theme_settings = $this->opts['theme_'.$theme];
		if (is_array($theme_settings)) $theme_settings = array();
		$html_input = array();

		// Precache options 
		foreach ($opts AS $var => $meta) {
			$html_input[$var] = array(
				"meta"=>$meta,
				"input_name" => ORTE_OPT_PREFIX."theme_".$theme."[".$var."]",
				"current" => isset($theme_settings[$var]) ? $theme_settings[$var] : $meta['default']
			);
		}

		// Get HTML output
		ob_start();
		require(dirname(__FILE__)."/view/admin.theme.settings.php");
		$output = ob_get_clean();

		return $output;
	}

	/**
	* Get settings for a template
	* @since 1.1
	* @param (string) theme folder name
	* @return (array) options from theme meta.php
	**/
	function get_theme_defaults ($theme="") {
		$ret = array();
		if (empty($theme)) return $ret;
		$themes = $this->list_valid_themes();
		if (isset($themes[$theme]) && isset($themes[$theme]['opts']) && is_array($themes[$theme]['opts'])) {
			foreach ($themes[$theme]['opts'] AS $key => $val) {
				$ret[$key] = $val['default'];
			}
		} return $ret;
	}

	/**
	* Get available post types from WP
	* @since 1.1
	* @param (boolean) if match set is filtered or not
	* @return (array) set of build in and custom post types
	**/
	function get_post_types ($filtered=false) {
		$types = get_post_types('','names');
		if ($filtered == false) $forbidden = array('attachment'=>1,'revision'=>1,'nav_menu_item'=>1,'feedback'=>1);
		$ret = array();

		foreach ($types AS $type) {
			if (!isset($forbidden[$type])) $ret[$type] = get_post_type_object($type);
		}
		
		return $ret;
	}

}
?>