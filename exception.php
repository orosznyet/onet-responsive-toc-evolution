<?php  

/**
* Custom Exception for importer
* This will make bug hunt much more easier. At least for this importer.
**/
class ONet_ResTOCEvo_Exception extends Exception {
	// Redefine the exception so message isn't optional
	public function __construct($message, $code = 0) {
		parent::__construct($message, $code);
	}

	// custom string representation of object
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\nStack trace:\n".$this->getTraceAsString();
	}
}

?>