jQuery(document).ready(function($) {
	var
	oabmMenu = jQuery("#menu-settings"),
	oabmMenuItem = oabmMenu.find("a[href$='"+orte_pointer_loc.menuname+"']"),
	oabmCollapseMenu = jQuery('#collapse-menu'),
	oabmOptions = {
		content: orte_pointer_loc.text,
		position: {
			edge: 'left',
			align: 'center',
			of: oabmMenu.is('.wp-menu-open') && !oabmMenu.is('.folded *') ? oabmMenuItem : oabmMenu
		},
		close: function() {
			clearInterval(oabmTimer);
			$.post( ajaxurl, {
				pointer: 'orte_point' + orte_pointer_loc.customid,
				action: 'dismiss-wp-pointer'
			});
		}
	},
	// Reposition when submenu opened ect.
	oabmReposition = function () {
		if (oabmMenu.hasClass("opensub")) { oabmOptions.position.of = oabmMenuItem; }
		else { oabmOptions.position.of = oabmMenu; }
		jQuery("body").pointer(oabmOptions);
	};

	// Reposition listeners 
	oabmTimer = setInterval(function () { oabmReposition() }, 200);

	// Display the pointer
	jQuery("body").pointer(oabmOptions).pointer("open");
});