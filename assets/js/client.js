$(document).ready(function () {
	window.postABM = jQuery("body,html").onetBookMark({
		container: "."+orte_client_loc.prefix+"post",
		pluginBase: orte_client_loc.pluginbase,
		correct: {
			topMargin: jQuery("#wpadminbar").length > 0 ? 30 : 0, // needed if Admin bar is displayed
			trailing: true,
			smallScreen: true
		},
		anim : {
			moveSpeed: 1000,
			moveSmart: true,
		},
		debug: true,
		display: true,
		disableOnShort: false,
		themeName : orte_client_loc.theme_name,
		themeSettings : orte_client_loc.theme_settings,
		headerText: orte_client_loc.header_text
	});
});