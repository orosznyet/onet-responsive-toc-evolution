jQuery.fn.extend({
onetBookMark: function(args){
	// We will use jQuery as $ inside. Poor Wordpress.
	var $ = jQuery,me = {};
	me.inited = false;


	/*
	@desc: Main function, init the whole stuff
	@param: void
	@return: (object) instance
	*/
	me.init = function () {
		// prevent reinit
		if (me.inited == true) return me;

		// Create init variable
		me.items = {}; // list of bookmark elements
		me.contMeta = {}; // content box metadatas (offest, height)
		me.inited = true; // Avoid reinit
		me.lastPixel = 0; // Last pixel updated
		me.customID = Math.round(Math.random()*10000); // Create custom ID for instance
		me.currentItem = -1; // The current item
		me.preventChange = false; // Prevent onscroll current item update. This value will not be overwritten but you can change with callbacks or on instance variable. Usefull for debug.
		me.themePath = ""; // Predefine for theme file
		me.themeVar = {}; // Predefine theme variables
		me.moveFinish = 0; // Time of last scroll finished
		me.disabled = false; // Is layout visibility is hidden
		me.terminated = false; // If script is terminated due to serious issue.
		me.listWrapMarginTop = 0; // Top margin correction
		me.activeHistory = []; // History of active items, basicly usefull for debug.
		me.themeLoaderVar = null;

		// Html elements container
		me.wrapBox = null; // Wrap box containing overlay and list
		me.listBox = null; // lister box, sub element of this.wrapBox
		me.listWrap = null; // this.listBox wrap object
		me.btnBox = null; // Button shortcut
		me.cont = null; // Container jQuery object
		
		// Grant "public" access from window object
		if (typeof window.onetBM !== 'object') window.onetBM = {}; // If this is the first instance we have to create public container.
		window.onetBM[me.customID] = me; // Okey, add current instance to public

		// Default settings variable
		var def = {
			pluginBase :				'/wp-content/plugins/onet_responsive_toc_evo', // where plugin is located, for example: 'http://www.webragacs.hu/wp-content/plugins/onet_responsive_toc_evo'
			theme :						'default', // theme name, will be called in the script as: pluginBase + '/themes/' + themeName
			container :					'.post', // container div, using only first matching element. For example: #cont
			target :					'body', // Append generated list to this match. for example: body
			selectors : {				// selector-class pairs inside of container. NOTE: All selectors are related to container!
				h1 :					'type1',
				h2 :					'type2',
				h3 :					'type3',
				h4 :					'type4',
				h5 :					'type5',
				h6 :					'type6'
				//,'.customTitle':	'customThemeClass' // You are free to se custom pairs (not just h1-6 pairs).
				},
			headerText :				'', // Header text for the listing (non clickable)
			display :					false, // display bookmark overlay as default
			disableOnShort :			4, // Disable the Bookmark layout if match set is shorter than provided number. Set false or zero to turn off this function.
			hotkeys : {					// set any of the followings to 0 to disable function. NOTE: one key for one action
				toggle :				84, // keyCode for toggle bookmark overlay (default: t)
				next :					78, // jump to next waypoint (default: n)
				back :					66 // jumt to previous waypoint (default: b)
				},
			updatePixel :				20, // scrollTop treshold in pixels
			anim : {
				moveSpeed :				200, // transition speed for scrolling
				moveEasing :			'easeInOutQuad', // Easing used by animate when moving viewport between waypoints/items
				moveSmart :				true, // Smart timing between close waypoints (this is usefull on slow speed)
				fadeSpeed :				200 // Fade in/out speed when toggle bookmark overlay (also can be done with css3 animation). Set Zero to turn off
			},
			correct : {
				topMargin :				0, // If you have top navigation bar you will love this opion
				removeHidden :			true, // Hide elements from itemlist which are hidden (balnk one will be autoremoved)
				trailing :				true, // Remove special chars from
				smallScreen :			true // Make active waypoint (link in bookmark overlay) always visible if screen is smaller than height of bookmar overlay.
			},
			debug :						false, // this is what it seems to be,
			embed : 					false, // set it true if you use this instance as embed (not as overlay)

			// Events
			'event' : {
				'itemsRefresh' :			function (instance,items) { return instance.items; }, // Execute code after waypoints are recreated, filter values. Must return with items object.
				'formatItem' :				function (text, item) { return text; }, // do you custom formating stuff
				'UIRefresh' :				function (instance) { return; }, // Do something when ui is refreshed
				'toggle' : 					function (instance) { return; } // Event after toggle
			}
		};


		// Apply custom variables
		me.opts = {};// OLD extend method: $.extend(def,typeof args !== 'undefined' ? args : {}); // if args is empty use empty object
		for (var op in def) {
			if (typeof args[op] !== "undefined") {
				if (typeof def[op] === 'object') { me.opts[op] = jQuery.extend(def[op],args[op]); }
				else { me.opts[op] = args[op]; }
			} else {
				me.opts[op] = def[op];
			}
		}

		// Post process some variables.
		me.themePath = me.opts.pluginBase + '/themes/' + me.opts.theme;
		me.opts.correct.topMargin = Math.abs(me.opts.correct.topMargin) * -1;
		me.opts.disableOnShort = parseInt(me.opts.disableOnShort);

		// Find container object and set as root. If no container stops working. NOTE: me.cont have to be jQuery object!
		if (me.opts.container.length > 0 && me.length != 1) me.cont = $(me.opts.container); // if container is not empty
		else if (typeof me[0] !== 'undefined') me.cont = me[0]; // else if try to go first matchin element from instance
		else {
			me.debug("No container. Failed to init.","INIT"); // Push debug notice.
			me.terminated = true;
			return false; // if no container simply return false;
		}

		// Load theme related resources
		if (typeof window.oABMThemes !== 'object') window.oABMThemes = {}; // create theme cache
		if (typeof window.oABMThemes[this.opts.theme] !== 'object') {
			$("head").append('<link rel="stylesheet" type="text/css" href="'+ me.themePath +'/styles.css" />');
			me.themeLoaderVar = $.ajax({
				url: me.themePath + "/methods.js",
				dataType: "script",
				success: function () {
					me.debug("Theme JS loaded: " + me.opts.theme,"INIT");
					me.themeVar = window.oABMThemes[me.opts.theme]; // create alias for theme container
				},
				error: function () {
					me.debug("No theme found. Failed to init.","INIT");
					me.terminated = true;
					me.disabled = true;
				}
			});
		}
		
		// Check if there is a problem with theme.
		if (typeof me.themeVar === 'undefined') { me.themeVar = {}; }
		
		// Okey create instance at the end
		me.getWayPoints();

		// Check if waypoint set is too short
		if (me.opts.disableOnShort > 0 && me.items.length <= me.opts.disableOnShort) {
			me.debug("Not enough waypoints (Required: "+me.opts.disableOnShort+"; Got: "+me.items.length+"). Failed to init.","INIT");
			me.terminated = true;
			me.disabled = true;
			return false;
		}

		// Refresh UI components
		me.refreshUI();

		// Attach custom events.
		$(window).bind("load.oabmLoaded" + me.customID, me, me.refreshUICore); // Event fired when page loading finished.
		$(document).bind("scroll.oabmScroll" + me.customID, me, me.onScroll); // Event fired on scroll
		$(document).bind("keydown.oabmNav" + me.customID, me, me.handleKeydown); // Event fired on scroll

		// Final checks
		if (me.terminated == true) return false; // Check if script is terminated.
		else {
			me.debug("Init successful","INIT"); // log merged options.
			return me; // finally retur the instance itself
		}
	};



	/*
	@desc: Top level debug/log code"AutoBookmarkLog"
	@param: log info, [n...]
	@return: boolean
	*/
	me.log = function () {
		if (arguments.length < 1 || !me.opts.debug) return false;
		for (var i=0;i<arguments.length;i++) window.console.log(arguments[i]);
		return true;
	};


	/*
	@desc: Advanced wrap for log
	@param: (string) statement, [(string) debug channel], [(any) debug object]
	@return: boolean
	*/
	me.debug = function (state, channel, debug_array) {
		if (typeof state !== "string") return false;
		var dbg = "ABM#"+me.customID+" "+ (typeof channel !== "undefined" ? channel+" "  : "") + state;
		if (typeof debug_array != "undefined") me.log(dgb,debug_array);
		else me.log(dbg);
		return true;
	}
	me.dbg = me.debug; // Alias



	/*
	@desc: Check if body/html is currently animated by this plugin
	@param: void
	@return: (boolean) if body is animated or not
	*/
	me.isMoving = function () {
		return (jQuery("body:animated,html:animated").length > 0 || me.moveFinish > (new Date().getTime()));
	}



	/*
	@desc: Recreate container metadatas
	@param: void
	@return: (object) Offsets (important for relative top calculations)
	*/
	me.getContainerMeta = function () {
		// Check if container is valid.
		if (typeof me.cont[0] === "undefined") {
			me.debug("Conteiner is invalid. TOC terminated.");
			me.terminated = true;
			return false;
		}

		// Generate content metadatas
		me.contMeta = {
			'top' :			me.cont[0].offsetTop + me.opts.correct.topMargin, // Top offset is no problem
			'bottom' :		me.cont[0].offsetTop + $(me.cont).outerHeight(true), // bottom offset is easy. top + outer height
			'innerTop' :	me.cont[0],
			'height' :		$(me.cont).height() // we use inner height for this one !
			};
		me.debug("Container meta recreated.");
		return me.contMeta;
	};



	/*
	@desc: Get margin-border-outline width on all side;
	@param: (jQuery object) item for space calculation
	@return: (object) item spacinggs
	*/
	me.calcSpace = function (itm) {
		var sides = ['Top','Right','Bottom','Left'], ret = {};
		for (var i=0;i<sides.length;i++) {
			ret[sides[i].toLowerCase()] = itm.cssInt("border"+ sides[i] +"Width") + itm.cssInt("padding"+ sides[i]) + itm.cssInt("margin"+ sides[i]);
		}
		return ret;
	};



	/*
	@desc: Get all bookmark items (Throw old waypoints and generate new.)
	@param: void
	@return: (array) menu items
	*/
	me.getWayPoints = function () {
		// Always recalc container infomations when called
		me.getContainerMeta();
		if (me.terminated == true) return false; // Check if script is terminated.
		// Erase collected datas
		me.items = [];
		
		// Start fetching those sections
		for (key in me.opts.selectors) {
			drop = me.cont.find(key); // get all matching elements from container
				
			for (var i=0;i<drop.length;i++) {
				var element = jQuery(drop[i]);
				if (element.text() == "" || (!element.is(":visible") && me.opts.removeEmptyOrHidden == true)) {
					// Catch blank or hidden
				} else {
					me.items[me.items.length] = {
						'top' :				element[0].offsetTop + me.opts.correct.topMargin, // top offset related to parent document
						'relTop' :			element[0].offsetTop-me.contMeta.top, // top offset related to container/parent
						'class' :			me.opts.selectors[key], // targer class for item
						'type' :			key, // Section type
						'elem' :			element // element info
						};
				}
			}
		}
		
		// Reorder elements (based on relTop offset)
		me.items.sort(function(x, y){
			return x.relTop - y.relTop;
		});

		// Okey, list generated, now 
		me.debug("Waypopint list recreated.");
		if (typeof me.opts.event.itemsRefresh === 'function') me.items = me.opts.event.itemsRefresh(me,me.items); // Apply user filter
		if (typeof me.themeVar.itemsRefresh === 'function') me.items = me.themeVar.itemsRefresh(me,me.items); // Apply user filter
		
		me.refreshUI();
		return me.items;
	};



	/*
	@desc: Scroll event (Using event data object to get active object.)
	@param: (object) event data
	@return: null;
	*/
	me.onScroll = function (e) {
		if (me.terminated == true) return; // Check if script is terminated.

		var 
			top = $(document).scrollTop(), // chace scrolltop value
			time = new Date().getTime(), // time with milisec
			lastID = -1;

		// Prevent update detection while body is being animated
		if (me.isMoving()) return;

		// Pixel update treshold - performance increase
		if (Math.abs(me.lastPixel-top) < me.opts.updatePixel || me.preventChange) return;

		// Get current element.
		for (var i=0;i<me.items.length;i++) {
			if (top < me.items[i].top) i = me.items.length;
			else lastID = i;
		}

		// Check if container is out of view
		if (me.contMeta.bottom < top) { lastID = -1; }

		// Log if current and old ID is different.
		if (me.currentItem != lastID) me.debug("Item change detected. Old: #"+ me.currentItem +" - New: #" + lastID,"SCROLL");

		// Update old variables
		me.lastPixel = top;
		me.currentItem = lastID;
		me.setActive(lastID,false);

		return null;
	};



	/*
	@desc: Handle hotkeys
	@param: (object) event data
	@return: null
	*/
	me.handleKeydown = function (e) {
		if (me.terminated == true) return; // Check if script is terminated.

		var keys = me.opts.hotkeys;

		// If target element is not body (we don't want to interrupt input fields)
		if (navigator.userAgent.match(/MSIE/i)) { // Yeah... new exception for IE. What a surprise.
			if (['input','select'].indexOf(e.delegateTarget.nodeName.toLowerCase()) > -1) { return null; }
		} else if (['body','html','#document','document'].indexOf(e.delegateTarget.nodeName.toLowerCase()) < 0) { return null; }

		// Handle keys
		if (e.keyCode == keys.toggle) { me.toggle(); }
		else if (e.keyCode == keys.next) { me.jumpNext(); }
		else if (e.keyCode == keys.back) { me.jumpPrev(); }
		else return; // if no key match return with null;

		me.debug("Keypress captured, keyCode: " + e.keyCode,"KEYDOWN");
		e.preventDefault();

		return null;
	};



	/*
	@desc: Handle click on each events
	@param: (object) event data
	@return: null
	*/
	me.handleClick = function (e) {
		var item = jQuery(e.target), id = item.data("itemID");

		e.preventDefault(); // Prevent default method for clicked elemet
		me.debug("Item clicked, Item ID: " + id,"CLICK"); // 
		me.jumpTo(id); // jump to target element
		return null;
	}



	/*
	@desc: Display or hide bookmark overlay
	@param: ([boolean] forced toggle status)
	@return: null
	*/
	me.toggle = function (status) {
		var both = jQuery.merge(me.wrapBox,me.btnBox);

		if (me.disabled == true) {
			both.hide();
			me.log("Currently disabled","UI");
		}
		if (me.wrapBox.hasClass("active") || status == false) both.removeClass("active").addClass("inactive");
		else both.addClass("active").removeClass("inactive");

		// Trigger events
		if (typeof me.opts.event.toggle === 'function') me.opts.event.toggle(me); // trigger event 
		if (typeof me.themeVar.toggle === 'function') me.themeVar.toggle(me); // trigger theme based ui refreshed
		me.debug("Layout toggled.");

		return null;
	};


	
	/*
	@desc: Shortcuts for me.jumpTo()
	@param: void
	@return: me.jumpTo()
	*/
	me.jumpPrev = function () { return me.jumpTo(me.currentItem-1); }
	me.jumpBack = function () { return me.jumpPrev(); }
	me.jumpNext = function () { return me.jumpTo(me.currentItem+1); }
	
	/*
	@desc: Jump to a list item
	@param: (int) target item ID
	@return boolean
	*/
	me.jumpTo = function (target) {
		if (typeof target !== 'number') return false;
		// check target validity
		if (target < 0) target = me.items.length - 1; // prevent prev() overflow
		else if (target >= me.items.length) target = 0; // prevent next() overflow

		me.debug("Jumped to: " + target + " From: " + me.currentItem);
		return me.setActive(target,true);
	}



	/*
	@desc: Set active item on the list
	@param: (int) target ID, ([boolean] move viewport or not)
	@return: boolean
	*/
	me.setActive = function (target,move) {
		// if target number missing, format invalid or does not exit return;
		if (typeof target !== "number" || (typeof me.items[target] !== 'object' && target != -1)) {
			me.debug("Can switch active, Invalid target ID: "+ target);
			return null;
		}
		// Prevent change if body is currently moving
		if (me.isMoving()) {
			me.debug("Currently moving, can't apply changes.");
			return null;
		}

		// Rearrange a ctive classes
		me.listBox.find("li.active").removeClass("active");
		me.listBox.find("li").filter(function() { return jQuery(this).data("itemID") == target; }).addClass("active");
		me.currentItem = target;

		// Some bugger variables
		if (me.activeHistory.length < 1 || me.activeHistory[me.activeHistory.length-1] != target) me.activeHistory.push(me.currentItem);

		// Move viewport to target waypoint
		if (move == true && me.currentItem >= 0) {
			var winHeight = jQuery(window).height(),
			item = me.items[me.currentItem],
			scrollTime = me.opts.anim.moveSpeed,
			distance = Math.abs(jQuery(document).scrollTop() - item.top);

			// Let's check distance from target and current scrollTop position
			if (distance < winHeight && me.opts.anim.moveSmart == true) scrollTime = Math.floor(scrollTime*(distance/(winHeight)));

			jQuery("html,body").stop().animate({scrollTop:item.top},scrollTime,me.opts.anim.moveEasing);
			me.moveFinish = (new Date().getTime()) + scrollTime; // overwrite previous animation finish time
			me.debug("Viewport moved to ITEM#" + me.currentItem + " Speed: " + scrollTime)
		}

		// Move currently active item into viewport (DOES NOT MOVING ACTUAL VIEWPORT)
		me.moveMenuIntoViewport();
		return true;
	};
	


	/*
	@desc: Move active menuitem to viewport
	@desc: This function is necessary if bookmark menu can not fit into viewport (higher than window height)
	@param: void
	@return: boolean
	*/
	me.moveMenuIntoViewport = function () {
		// Check if smallScreen correction is active
		if (me.opts.correct.smallScreen == false) return false;
		
		// Load target element
		var targetElem = me.listBox.find("li.active"); // target element
		if (targetElem.length < 1) return false;

		// Prevent "renimate" listing
		if (me.listBox.is(":animated")) return false;

		// If list is smaller than screen
		if (me.listWrap.outerHeight() < jQuery(window).height()) {
			if (me.listWrap.cssInt("marginTop") != me.listWrapMarginTop)
				me.listWrap.stop().animate({marginTop:me.listWrapMarginTop},150); // reset default top margin
			return false;
		}

		// If the menu does not fit in listBox
		var winHeight = $(window).height(),
			scrollTop = $(document).scrollTop(),
			activeElem = me.listBox.find("li.active"),
			activeItem = me.items[activeElem.data("itemID")],
			startLock = parseInt( winHeight / 2),
			endLock = parseInt( me.listWrap.height() - winHeight + 10),
			targetMargin = parseInt(activeItem.mTop - ( winHeight / 2 ));

		// If active item can not be found return false of course
		if (typeof activeItem === "undefined" ) return false;

		// check for overflows
		if (activeItem.mTop < startLock) targetMargin = me.listWrapMarginTop;
		else if (targetMargin > endLock ) targetMargin = endLock * -1;
		else targetMargin = targetMargin * -1;
		
		// Animate list box
		me.listWrap.stop().animate({marginTop: targetMargin },150);

		return true;
	};



	/*
	@desc: Generate position values for each menu item
	@param: void
	@return: (array) menu item positions
	*/
	me.itemRePos = function () {
		var
			mRel = me.listBox.children("li").first().offset().top,
			mHeight = me.listWrap.height();

		for (var i=0;i<me.items.length;i++) {
			var elem = $(me.items[i].elem), mElem = $(me.items[i].mElem); // element object in CONTENT
			me.items[i].elem = elem; // update element obj
			me.items[i].mElem = mElem; // update element obj
			me.items[i].top = elem.offset().top + me.opts.correct.topMargin; // Position realted to DOCUMENT
			me.items[i].relTop = elem.offset().top-me.contMeta.top; // Relative top position inside container
			me.items[i].mTop = mElem.offset().top - mRel;
			me.items[i].mPerc = Math.ceil(me.items[i].mTop / mHeight * 100);
		}
		me.debug("Item related number recalculated");
		return me.items;
	}



	/*
	@desc: Refresh UI
	@desc: Recalc waypoints and redraw ui elements (this function does the job! Not refreshUI )
	@desc: Used directly only on events. You can use me.refreshUI() on instance object.
	@param: void
	@return:boolean
	*/
	me.refreshUI = function () { me.refreshUICore(); }; // Mask for inner usage
	me.refreshUICore = function (e) {
		if (me.terminated == true) return false; // Check if script is terminated.

		var id = "oabm" + me.customID, btn = "oabmBtn" + me.customID;

		// Destroy old container and create new one
		jQuery("#" + id +  ",#"+ btn).remove();
		jQuery("<div />").attr({'id':id, 'class': "oabmt-"+ me.opts.theme +" wrap "+ (me.opts.display == true ? "" : "in") +"active"}).prependTo(me.opts.target);
		me.wrapBox = jQuery("#" + id);
		me.wrapBox.append('<div class="overlay" /><ul class="list" />'); // add som inner content
		me.listBox = me.wrapBox.find(".list");
		
		// Now go on with the button
		jQuery("<div />").attr({'id':btn, 'class' : "oabmt-"+ me.opts.theme +" btn "+ (me.opts.display == true ? "" : "in") +"active"}).insertAfter(me.wrapBox);
		me.btnBox = jQuery("#" + btn);
		me.btnBox.append("<span />"); // Add span for text
		me.btnBox.find("span").click(me,me.toggle); // bind event

		// Add shift item to list object
		if (typeof me.opts.headerText === 'string' && me.opts.headerText.length > 0) {
			jQuery("<li />").
				attr({'class' : "itemText"}).
				html(me.opts.headerText).
				appendTo(me.listBox);
		}

		// Generate HTML verion of list and fill the .wrapBox.list
		for (var i=0;i<me.items.length;i++) {
			drop = me.items[i];
			me.items[i].mElem = jQuery("<li />").
				attr({'class' : "item " + drop.class + " " + drop.type + (me.currentItem == i ? " active" : "")}).
				html(me.itemFormatter(drop.elem.text())).
				click(me,me.handleClick).
				data({itemID:i}).
				appendTo(me.listBox);
		}

		// Add list wrap element
		me.listBox.wrap("<div class='listWrap'/>");
		me.listWrap = me.wrapBox.find(".listWrap").append(jQuery("<div/>").css({clear:"both"}));

		me.listWrapMarginTop = me.listWrap.cssInt("marginTop");

		me.debug("Refreshed.","UI");
		me.itemRePos();
		if (typeof me.opts.event.UIRefresh === 'function') me.opts.event.UIRefresh(me); // trigger event 
		if (typeof me.themeVar.UIRefresh === 'function') me.themeVar.UIRefresh(me); // trigger theme based ui refreshed
		
		// Check if layout is disabled or not
		if (me.disabled == true) {
			jQuery.merge(me.wrapBox,me.btnBox).hide();
			me.log("Currently disabled","UI");
		}

		return true;
	};



	/*
	@desc: Format listed elements text 
	@param: (string) text of list item
	@return (string) formated text of list item
	*/
	me.itemFormatter = function (txt) {
		txt = me.opts.correct.trailing ? txt.replace(/(\:|\.|\!|\?|\-)$/gi,'') : txt;
		return typeof me.opts.event.formatItem === 'function' ? me.opts.event.formatItem(txt) : txt;
	};



	// at the end do the init stuff.
	return me.init();
}
});


// Get sizes in int using default jQuery css() method
// Works with px, em
jQuery.fn.extend({
cssInt: function(arg){
	var work = null;
	
	// We can handle only one single element.
	if (this.length > 0 && typeof this[0] !== 'undefined') work = this[0];
	else return null;
	
	// accepted only one arg as string. NO object.
	if (typeof arg !== 'string') return null;
	
	// if no number in paired value
	if (!jQuery(work).css(arg).match(/[0-9]/i)) return null;

	// do the "magic"
	return parseInt(jQuery(work).css(arg).replace(/(px|em)$/gi,''));
}
});

/** NOT FINISHED **/
jQuery.extendDeep = function (objs,level) {
	if (typeof objs === "undefined") return; // if no incoming objects return null
	if (typeof level === "undefined") level = 0; // if level is undefined set it to zero

	var retrive = ''; // returning array/object

	for (var obj in objs) {
		if (typeof objs[objs] !== "undefined") {
			if (typeof def[op] === 'object') { this.opts[op] = jQuery.extend(def[op],args[op]); }
			else { this.opts[op] = args[op]; }
		} else {
			this.opts[op] = def[op];
		}
	}

	return retrive;
};

// Add Additional easing for OABM
jQuery.extend(jQuery.easing, {
	easeInOutQuad: function (a, b, c, d, e) {
		if ((b/=e/2) < 1) return d/2*b*b + c;
		return -d/2 * ((--b)*(b-2) - 1) + c;
	}
});