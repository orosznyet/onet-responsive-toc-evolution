var $ = jQuery; // As always, alias

/**
* Update UP based on theme settings
* @since 0.1
**/
window.orteUpdateActiveTheme = function () {
	$(".hideIfThemeChange").hide();
	if (!$("input#"+orte_admin_loc.prefix+"global").is(":checked")) return;
	current = $("input[name='"+orte_admin_loc.prefix+"theme']").filter(function () { return this.checked != '' ? true : false; }).val();

	if (typeof current !== "undefined" && current.length > 0) {
		$(".cont-theme-"+current).show();
		$(".cat-themeset").show();
	}
	else $(".cat-themeset").hide();
	return current;
}

/**
* Update form elements displays (global)
* @since 0.1
**/
window.orteAdminCheckForm = function () {
	var s = orte_admin_loc.prefix;
	// Start with global chec
	if ($("input#"+orte_admin_loc.prefix+"global").is(":checked")) { $(".hideIfNoGlobal").show(); }
	else { $(".hideIfNoGlobal").hide(); }
	window.orteUpdateActiveTheme();
	return true;
}

/**
* Vertical align (for labels)
* @since 0.1
**/
$.fn.vAlign = function() {
	return this.each(function(i){
		var
			me = $(this),
			ah = me.height(),
			ph = me.parent().height(),
			mh = (ph - ah) / 2;
		me.css('margin-top', mh);
	});
};

/**
* Run the following script if the page is settings page.
**/
if (pagenow == "settings_page_onet-responsive-toc-evo")
$(document).ready(function () {
	$("input[name='"+orte_admin_loc.prefix+"theme']").click(function () { window.orteUpdateActiveTheme(); });

	// Vertical align left side
	$(".onet-admin-panel .row .left").vAlign();

	$(".parse-onet-onof").each(function () {
		// Button texts
		if ($(this).hasClass("onet-yesno")) {
			var buttons = {
				btn1 : orte_admin_loc.lang.yes,
				btn2 : orte_admin_loc.lang.no
			};
		} else {
			var buttons = {
				btn1 : orte_admin_loc.lang.on,
				btn2 : orte_admin_loc.lang.off
			};
		}

		// Add custom UI
		$('<input type="button" class="onet-button on '+$(this).prop("id")+'" id="'+$(this).prop("id")+'_on" value="'+buttons.btn1+'" />'+
			'<input type="button" class="onet-button off '+$(this).prop("id")+'" id="'+$(this).prop("id")+'_off" value="'+buttons.btn2+'" />').
		insertBefore($(this));
		// Hide checkbox, comment for debug
		$(this).hide();

		// Add checkbox wathcer
		$(this).change(function () {
			$(".onet-button." + $(this).prop("id")).removeClass("active");
			$("#" + $(this).prop("id") + "_" + ($(this).is(":checked") ? "on" : "off")).addClass("active");
			// Refresh UI on every change.
			setTimeout(window.orteAdminCheckForm,50);
		});
		// Trigger watcher to refresh custom UI
		$(this).change();
	});

	// Add custom switch ui handlers
	$(".onet-button.on, .onet-button.off").click(function () {
		var item = $("#" + $(this).prop("id").replace(/_on$|_off$/gi,''));
		if ($(this).hasClass("on")) item.prop({checked:"checked"});
		else  item.removeAttr("checked");
		item.change();
		delete(item);
	});

	// Label correction for input name instead of id
	$("label.onet-radioName").click(function (e)  {
		var items = $("input[name='"+$(this).attr("for")+"']");
		if (items.length < 1) return;
		newItem = null;

		// Check if there is 
		if ($("input[name='"+$(this).attr("for")+"']").filter(":checked").length < 1) {
			newItem = items[0];
			return;
		} else {
			// Selected radio button search
			for (var i=0;i<items.length;i++) {
				// Find checked radiobutton
				if (items[i].checked == true) {
					$(items[i]).parent().removeClass("checked");
					if ((i+1) == items.length) newItem = items[0];
					else  newItem = items[i+1];
					i = items.length+1;
				}
			}
		}

		// Trigger change
		$(newItem).click().parent().addClass("checked");
	});

	$(".onet-radioWrap > input[type='radio']").click(function (e) {
		// Do not execute if event is not intentional
		if (e.isTrigger != true) {
			var me = $(e.target); group = me.attr("name");
			$("input[name='"+group+"']").parent().removeClass("checked");
			me.parent().addClass("checked");
		}
	});

	// Refresh UI
	window.orteAdminCheckForm();
});