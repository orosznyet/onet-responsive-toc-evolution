<div class="wrap">
	<h2 class="onet-admin-header"><?php _e("ONet Responsive TOC Evolution", "orte"); ?> <small><?php echo ORTE_VER; ?></small></h2>

	<?php if (count($updates)) : ?>
		<div id="message" class="updated below-h2">
			<?php foreach($updates AS $update) : ?><p><?php echo $update; ?></p><?php endforeach; ?>
		</div>
	<?php endif; ?>

	<!-- Secondary coll -->
	<div class="postbox-container onet-right-block">
		<div class="metabox-holder">	
			<div class="meta-box-sortables">
				<?php $pass->admin_custometa_premiumsupport(); ?>
				<?php $pass->admin_custometa_support(); ?>
			</div>
		</div>
	</div>

	<!-- Main col -->
	<div class="postbox-container onet-left-block">
		<div class="metabox-holder">	
			<div class="meta-box-sortables">
				<form method="post">
					<?php wp_nonce_field('orte_nonce'.$pass->opts['custom_id'], 'orte_nonce_validator', false ); ?>
					
					<div style="clear:both"></div>
					<div id="oABM-settings" class="onet-admin-panel">
						<h1 class="category cat-general"><?php _e("General settings","orte"); ?></h1>
						<!-- General settings -->
						<div class="content cont-general">
							<!-- Global -->
							<div class="row">
								<div class="left">
									<label for="<?php echo $prefix; ?>global"><?php _e("Enable TOC overlay","orte"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo $prefix; ?>global" id="<?php echo $prefix; ?>global" class="parse-onet-onof" <?php echo $pass->opts['global'] == 1 ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("You can turn of TOC with one single click at anytime.","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Global -->

							<!-- Display TOC for custom post types -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label><?php _e("Display for post types/pages","orte");?></label>
								</div>
								<div class="right">
									<?php foreach($postTypes AS $type => $meta) : ?>
										<p>
											<input type="checkbox" name="<?php echo $prefix; ?>displayfor[<?php echo $type; ?>]" id="<?php echo $prefix; ?>displayfor_<?php echo $type; ?>" class="parse-onet-onof" <?php echo in_array($type, $pass->opts['displayfor']) ? " checked='checked'" : ""; ?> />
											<label for="<?php echo $prefix; ?>displayfor_<?php echo $type; ?>" class="sublabel"><?php echo $meta->labels->name; ?></label>
											<span class="onet-help">
												<span class="text">?</span>
												<span class="bubble"><?php _e("You can overwrite this option for each post/page individually.","orte"); ?></span>
											</span>
										<p>
									<?php endforeach; ?>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Display TOC for custom post types -->
							
							<!-- Title -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>title"><?php _e("Title text","orte"); ?></label>
								</div>
								<div class="right">
									<input type="input" name="<?php echo $prefix; ?>title" id="<?php echo $prefix; ?>title" class="onet-input" value="<?php echo $pass->opts['title']; ?>" />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Text for the header element of the TOC overlay. Default value: Bookmarks","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Title -->

						</div><!-- Global settings -->


						<h1 class="category cat-theme hideIfNoGlobal">
							<?php _e("Available themes","orte"); ?>
						</h1>
						<!-- Theme settings -->
						<div class="content cont-theme hideIfNoGlobal">
							<!-- Global -->
							<div class="row">
								<div class="left">
									<label for="<?php echo $prefix; ?>theme" class="onet-radioName"><?php _e("Click on the theme you want to use","orte"); ?></label>
								</div>
								<div class="right advanced">
									<?php if (count($themes) < 1) : ?>
										<span class='error'><?php _e("Sorry, there are no valid theme. Try to reinstall the stock plugin.","orte"); ?></span>
									<?php else : ?>
										<?php foreach ($themes AS $theme => $meta) : ?>
											<label class="onet-radioWrap <?php echo $pass->opts['theme'] == $theme ? ' checked' : ""; ?>">
												<input type="radio" name="<?php echo $prefix; ?>theme" value="<?php echo $theme; ?>" <?php echo $pass->opts['theme'] == $theme ? ' checked="checked"' : ""; ?> />
												<span class="title"><?php echo $meta['name']; ?></span>
												<img src="<?php echo ORTE_THEME_URL."/".$theme."/screen.jpg"; ?>" alt="" title="" />
											</label>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
								<div style="clear:both"></div>
							</div>
						</div><!-- // Theme settings -->

						<h1 class="category cat-themeset hideIfNoGlobal">
							<?=__("Theme related settings","orte")?>
						</h1>
						<?php foreach ($theme_settings AS $theme => $html) echo $html; ?>

						<h1 class="category cat-advanced hideIfNoGlobal">
							<?php _e("Advanced settings","orte"); ?>
						</h1>
						<!-- Advanced settings -->
						<div class="content cont-advanced hideIfNoGlobal">
							<!-- Global -->
							<!-- Auto Open -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>autoopen"><?php _e("Show TOC by default","orte"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo $prefix; ?>autoopen" id="<?php echo $prefix; ?>autoopen" class="parse-onet-onof" <?php echo $pass->opts['autoopen'] == 1 ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Show TOC overlay on each available content by default. You can overwrite this option for each content individually.","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Auto Open -->

							<!-- Open on Swipe -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>swipeopen"><?php _e("Enable swipe on mobile devices","orte"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo $prefix; ?>swipeopen" id="<?php echo $prefix; ?>swipeopen" class="parse-onet-onof" <?php echo $pass->opts['swipeopen'] == 1 ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Display TOC overlay on swipe. Available only on touch devices. This is a global value which means you can not change this option for each content.","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Open on Swipe -->

							<!-- Swipe origin -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>swipeorigin"><?php _e("Swipe origin","orte"); ?></label>
								</div>
								<div class="right">
									<select name="<?php echo $prefix; ?>swipeorigin" id="<?php echo $prefix; ?>swipeorigin" class="onet-input" style="width:150px">
										<option<?php echo $pass->opts['swipeorigin'] == "left" ? ' selected="selected"' : ""; ?> value="left"><?php _e("From left side","orte"); ?></option>
										<option<?php echo $pass->opts['swipeorigin'] == "right" ? ' selected="selected"' : ""; ?> value="right"><?php _e("From right side","orte"); ?></option>
									</select>
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("You can set the Swipe origin if swipe feature is enabled on mobile devices.","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Swipe origin -->

							<!-- Disable "open" button -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>hidebutton"><?php _e("Hide button","orte"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo $prefix; ?>hidebutton" id="<?php echo $prefix; ?>hidebutton" class="parse-onet-onof" <?php echo $pass->opts['hidebutton'] == 1 ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Hide display button.","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Display "open" button -->

							<!-- Enable hotkeys -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>hotkeys"><?php _e("Enable navigation via keyboard","orte"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo $prefix; ?>hotkeys" id="<?php echo $prefix; ?>hotkeys" class="parse-onet-onof" <?php echo $pass->opts['hotkeys'] == 1 ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Enable navigation via keyboard. T - toggle, b - previous/back, n - next","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Enable hotkeys -->
							<!-- Manual init -->
							<div class="row hideIfNoGlobal">
								<div class="left">
									<label for="<?php echo $prefix; ?>manual"><?php _e("Manual init","orte"); ?></label>
								</div>
								<div class="right">
									<input type="checkbox" name="<?php echo $prefix; ?>manual" id="<?php echo $prefix; ?>manual" class="parse-onet-onof" <?php echo $pass->opts['manual'] == 1 ? "checked='checked'" : ""; ?> />
									<span class="onet-help">
										<span class="text">?</span>
										<span class="bubble"><?php _e("Include script but do not autorun. Can be overwritten for each posts/pages.","orte"); ?></span>
									</span>
								</div>
								<div style="clear:both"></div>
							</div><!-- // Manual init -->
						</div><!-- // Advanced settings -->


					</div>

					<input type="submit" class='button-primary' name="<?php echo $prefix; ?>_update" value="<?php _e( 'Save Changes',"orte"); ?>" style="margin: 30px 10px;" />
					<div style='clear:both;'></div>
				</form>
			</div>
		</div>
	</div>
	
	<div style="clear:both;"></div>

</div>