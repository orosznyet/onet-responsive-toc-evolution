<p>
	<label for="<?php echo ORTE_OPT_PREFIX; ?>single_overlay"><?php _e( 'Use overlay', 'orte'); ?></label>
	<select id="<?php echo ORTE_OPT_PREFIX; ?>single_overlay" name="<?php echo ORTE_OPT_PREFIX; ?>single_overlay">
		<option value="0" <?php echo $opts['overlay'] == 0 ? "selected='selected'" : ""; ?>><?php _e( 'Inherited', 'orte'); ?></option>
		<option value="1" <?php echo $opts['overlay'] == 1 ? "selected='selected'" : ""; ?>><?php _e( 'Yes', 'orte'); ?></option>
		<option value="2" <?php echo $opts['overlay'] == 2 ? "selected='selected'" : ""; ?>><?php _e( 'No', 'orte'); ?></option>
	</select>
</p>
<p><strong><?php _e( 'Advanced options', 'orte'); ?></strong></p>
<p>
	<label for="<?php echo ORTE_OPT_PREFIX; ?>manual_init"><?php echo __( 'Manual init', 'orte'); ?></label>
	<select id="<?php echo ORTE_OPT_PREFIX; ?>manual_init" name="<?php echo ORTE_OPT_PREFIX; ?>manual_init">
		<option value="0" <?php echo $opts['manual'] == 0 ? "selected='selected'" : ""; ?>><?php _e( 'Inherited', 'orte'); ?></option>
		<option value="1" <?php echo $opts['manual'] == 1 ? "selected='selected'" : ""; ?>><?php _e( 'No', 'orte'); ?></option>
		<option value="2" <?php echo $opts['manual'] == 2 ? "selected='selected'" : ""; ?>><?php _e( 'Yes', 'orte'); ?></option>
	</select>
	
</p>