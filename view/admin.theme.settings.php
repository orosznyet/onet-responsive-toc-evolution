<div class="content cont-theme-<?php echo $theme?> hideIfNoGlobal hideIfThemeChange">
	<?php foreach ($html_input AS $var => $val) : ?>
	<div class="row hideIfNoGlobal">
		<div class="left"><label for="<?php echo $val['input_name']; ?>"><?php echo $val['meta']['title']; ?></label></div>
		<div class="right">
			<?php if ($val['meta']['type'] == "select") : ?>
				<!-- selectbox -->
				<select type="checkbox" name="<?php echo $val['input_name']; ?>" id="<?php echo $val['input_name']; ?>" class="onet-input" style="width:150px;">
				<?php foreach ($val['meta']['options'] AS $opt => $text) : ?>
					<option value="<?php echo $opt; ?>" <?php echo $opt == $val['current'] ? ' selected="selected"' : ''; ?>><?php echo $text; ?></option>
				<?php endforeach; ?>
				</select>
			<?php elseif ($val['meta']['type'] == "checkbox") : ?>
				<!-- checkox -->
				<input type="checkbox" name="<?php echo $val['input_name']; ?>" id="<?php echo $val['input_name']; ?>" class="parse-onet-onof" <?php echo $val['current'] == 1 ? ' checked="checked"' : ''; ?> />
			<?php endif; ?>
					
			<!-- Add help box if neccessary -->
			<?php if (isset($val['meta']['help']) && $val['meta']['help'] != "") : ?>
				<span class="onet-help"><span class="text">?</span><span class="bubble"><?php echo $val['meta']['help']; ?></span></span>
			<?php endif; ?>
		</div>
		<div style="clear:both"></div>
	</div>
	<?php endforeach; ?>
</div>