<?php

$settings = array(
	"name" => __("Over Layer","orte"),
	"author" => "Koller Jozsef",
	"email" => "orosznyet@gmail.com",
	"opts" => array(
		"scheme" => array(
			"title" => __("Color Scheme","orte"),
			"type" => "select",
			"default" => "light",
			"options" => array(
				"light" => __("Light","orte"),
				"dark" => __("Dark","orte"),
				"blue" => __("Blue","orte"),
				"yellow" => __("Yellow","orte"),
				"green" => __("Green","orte"),
				"red" => __("Red","orte")
			)
		),
		"button" => array(
			"title" => __("Button design","orte"),
			"type" => "select",
			"default" => "plus",
			"options" => array(
				"plus" => __("Plus","orte"),
				"book" => __("Book","orte"),
				"ribbon" => __("Ribbon","orte")
			)
		),
		"buttonver" => array(
			"title" => __("Button vetical position","orte"),
			"type" => "select",
			"default" => "bottom",
			"options" => array(
				"top" => __("Top","orte"),
				"bottom" => __("Bottom","orte"),
				"middle" => __("Middle","orte")
			)
		),
		"buttonhor" => array(
			"title" => __("Button horizontal position","orte"),
			"type" => "select",
			"default" => "right",
			"options" => array(
				"left" => __("Left","orte"),
				"right" => __("Right","orte"),
				"center" => __("Center","orte"),
			)
		)
	)
);

?>