window.oABMThemes.default = {
	// Events
	UIRefresh: null, // UI refresh event
	itemRefresh: null, // event after recalculated itemlist
	toggle: null, // when overlay toggled

	// Settings
	stretchOverlay: false, // Stretch overlay to window size. Also you can do this with simple CSS
	floatOverlay: true, // Floated overay autorealign by script!
};